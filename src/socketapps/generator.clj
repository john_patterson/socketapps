(ns socketapps.generator
  (:require [clojure.java.io :as io]
            [clojure.string :as s]
            [clojure.walk :refer [keywordize-keys]]
            [clojure.data.json :as json]
            [socketapps.common :as c]))

(defn- render-template
  [record]
  (let [template (slurp (io/resource c/template-path))]
    (-> template
        (s/replace #"\$\$port\$\$" (str (:port record)))
        (s/replace #"\$\$app-name\$\$" (str (:app record))))))

(defn- rendered-path
  [app]
  (str c/template-out app ".conf"))

(defn- save-rendered-template
  [app template]
  (let [path (rendered-path app)]
    (spit path template)))

(defn write-site-configuration
  [record]
  (save-rendered-template (:app record) (render-template record)))

(defn file->extension
  [file]
  (second (s/split (.getName file) #"\.")))

(defn file->name
  [file]
  (first (s/split (.getName file) #"\.")))

(defn file-is-configuration
  [file]
  (let [extension (file->extension file)]
    (and (not (nil? extension))
         (= (s/lower-case extension) "conf"))))

(defn current-configurations
  []
  (->> c/template-out
      io/file
      file-seq
      (filter file-is-configuration)
      (map file->name)))

(defn template-configuration-exists?
  [record]
  (-> record
      :app
      rendered-path
      io/as-file
      io/.exists))

(defn delete-site-configuration
  [record]
  (if (template-configuration-exists? record)
    (let [path (rendered-path (:app record))]
      (-> record
          :app
          rendered-path
          io/as-file
          io/delete-file))))

(defn read-sites
  []
  (map #(keywordize-keys %)
    (-> c/sites-path
        io/resource
        (slurp)
        (json/read-str))))

(defn- write-site-file
  [contents]
  (let [site-file (io/resource c/sites-path)]
    (->> contents
         json/write-str
         (spit site-file))))

(defn remove-site
  [site]
  (let [sieve (fn [row] (not= (:app row) (:app site)))]
    (write-site-file (filter sieve (read-sites)))))

(defn edit-site
  [site]
  (let [sieve (fn [row] (not= (:app row) (:app site)))]
    (write-site-file (filter sieve (read-sites)))))

(defn add-site
  [site]
  (-> (read-sites)
      (into [site])
      write-site-file))

(defn site-exists
  [site]
  (->> (read-sites)
       (filter #(= (:app %) (:app site)))
       (count)
       (< 0)))
