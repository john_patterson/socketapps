(ns socketapps.handler
  (:require
   [compojure.core :as cc]
   [compojure.route :as route]
   [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
   [ring.util.anti-forgery :refer [anti-forgery-field]]
   [ring.util.response :refer [response content-type redirect]]
   [hiccup.middleware :refer [wrap-base-url]]
   [hiccup.page :as hic-p]
   [hiccup.element :as e]
   [hiccup.form :as f]
   [hiccup.util :as u]
   [socketapps.links :as link]
   [socketapps.generator :as gen]))

(defn make-page
  [title body]
  (hic-p/html5
   [:head
    [:title title]
    (hic-p/include-js
     "//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js")
    (hic-p/include-css
     "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css")
    (hic-p/include-js
     "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js")]
   body))

(defn to-list
  [ls]
  (let [wrap (fn [x] [:li x])]
    [:ul (map wrap ls)]))

(defn print-site-record
  [row]
  [:div
   (e/link-to (link/link-app row)
              (str "App Name: " (:app row) ", Port: " (:port row)))
   (e/link-to (link/link-remove row)
              " [X]")])

(defn print-no-conf
  [row]
  [:div
   (str (:app row) " ")
   (e/link-to (link/link-generate-conf row)
              "[Generate]")])

(defn print-unmanaged
  [file-name]
  [:div
   (str file-name " ")
   (e/link-to (link/link-remove-conf {:app file-name})
              "[Remove]")])

(def break [:br])

(defn coll-diff
  [coll1 coll2]
  (let [c1 (set coll1)
        c2 (set coll2)]
    (seq (clojure.set/difference c1 c2))))

(defn labelled-text [key pretty]
  [:div.form-group.row
   (f/label {:class "col-xs-2 col-form-label"}
            key
            pretty)
   [:div.col-xs-10
    (f/text-field key)]])

(defn print-sites-missing-conf
  [sites]
  (let [dne #(not (gen/template-configuration-exists? %))
        missing (filter dne sites)]
    (if (not (empty? missing))
      [:div "Sites missing conf: " break
       (to-list (map print-no-conf missing))]
      break)))

(defn print-unmanaged-conf
  [sites]
  (let [confs (gen/current-configurations)
        unmanaged (coll-diff confs (map :app sites))]
    (if (not (empty? unmanaged))
      [:div "Unmanaged confs: " break
       (to-list (map print-unmanaged unmanaged))]
      break)))

(defn homepage []
  (let [sites (gen/read-sites)]
    (make-page "Sockets Home"
               [:div
                [:div "Current sites:" break
                (to-list (map print-site-record sites))]
                break
                (print-sites-missing-conf sites)
                (print-unmanaged-conf sites)
                [:div "New site:" break
                 (f/form-to [:post (u/to-uri "/New")]
                            (labelled-text "app" "App Name: ")
                            (labelled-text "port" "Port: ")
                            (anti-forgery-field)
                            (f/submit-button {:class "btn btn-primary"} "Create"))]])))

(defn si->int
  "Make a string or an integer into an integer"
  [input]
  (cond
    (string? input) (read-string input)
    (number? input) input))

(defn validate
  "Return false if app exists or port is non-numeric.
  Else return the new record."
  [app port]
  (let [rec {:app app :port (si->int port)}]
    (and (number? (:port rec))
         (not (gen/site-exists rec))
         rec)))

(defn new-app
  "Maps to /New path"
  [app port]
  (let [rec (validate app port)]
    (if rec
      (do (gen/add-site rec)
          (gen/write-site-configuration rec)))))


(defn remove-app
  "Maps to /Remove/:app"
  [app]
  (let [rec {:app app}]
    (if (gen/site-exists rec)
      (do (gen/remove-site rec)
          (gen/delete-site-configuration rec)))))

(cc/defroutes all-routes
  (cc/GET "/" []
    (homepage))
  (cc/POST "/New" [app port]
    (new-app app port)
    (redirect "/"))
  (cc/GET "/GenerateConf/:app" [app]
    (->> (gen/read-sites)
         (filter #(= (:app %) app))
         first
         gen/write-site-configuration)
    (redirect "/"))
  (cc/GET "/RemoveConf/:file" [file]
    (gen/delete-site-configuration {:app file})
    (redirect "/"))
  (cc/GET "/Remove/:app" [app]
    (remove-app app)
    (redirect "/")))


(def application-routing
  (wrap-base-url all-routes))

(def app
  (wrap-defaults application-routing site-defaults))
