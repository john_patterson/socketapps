(ns socketapps.links
  (:require [socketapps.common :as c]))

(defn link-app
  [record]
  (str "http://" c/server-name ":" (:port record) "/" (:app record)))

(defn link-remove
  [record]
  (str "/Remove/" (:app record)))

(defn link-generate-conf
  [rec]
  (str "/GenerateConf/" (:app rec)))

(defn link-remove-conf
  [rec]
  (str "/RemoveConf/" (:app rec)))
