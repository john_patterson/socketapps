(ns socketapps.common
  (:require [clojure.java.io :as io]
            [clojurewerkz.propertied.properties :as p]))

(defn get-property
  [prop]
  (-> "config.properties"
      io/resource
      p/load-from
      (get-in [prop])))

(def server-name
  (get-property "server"))

(def template-out
  (get-property "output"))

(def template-path "location.template")
(def sites-path "sites.json")
